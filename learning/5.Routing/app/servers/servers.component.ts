import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ServersService } from './servers.service';

@Component({
  selector: 'app-servers',
  templateUrl: './servers.component.html',
  styleUrls: ['./servers.component.css'],
})
export class ServersComponent implements OnInit {
  public servers: { id: number; name: string; status: string }[] = [];

  constructor(private serversService: ServersService, private router: Router,private route:ActivatedRoute) {}

  ngOnInit(): void {
    console.log('it is called');
    this.servers = this.serversService.getServers();
  }

  onReload() {
    // we can also put only navigate() when we want to just load the page withlocalhost/route
    /* ie whenever we use navigate function it redirect it to localhost/givenRoute
      but if we want to route the page relative to the current page we use relativeto argument
      where page will be reloaded as localhost/servers/servers
    */
    this.router.navigate(['servers'],{relativeTo:this.route});
  }
}
