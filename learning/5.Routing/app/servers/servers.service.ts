export class ServersService {
  private servers = [
    {
      id: 1,
      name: 'Productionserver',
      status: 'online',
    },
    {
      id: 2,
      name: 'Testserver',
      status: 'offline',
    },
    {
      id: 3,
      name: 'Devserver',
      status: 'offline',
    },
  ];

  getServers() {
    console.log("runned the servers");
    return this.servers;
  }

  getServer(givenId: number) {
    // const server: { id: number; name: string; status: string }=
    //   this.servers.find((s) => {
    //     return s.id === id;
    //   });

    var ser = this.servers.find(function (obj) {
      return obj.id === givenId;
    });
    console.log(ser);
    return {
      id: 1,
      name: 'Productionserver',
      status: 'online',
    };
  }

  updateServer(id: number, serverInfo: { name: string; status: string }) {
    const server = this.servers.find((s) => {
      return s.id === id;
    });
    if (server) {
      server.name = serverInfo.name;
      server.status = serverInfo.status;
    }
  }
}
