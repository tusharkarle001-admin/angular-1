import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css'],
})
export class UserComponent implements OnInit {
  user: { id: number; name: string };

  constructor(private route:ActivatedRoute) {
    console.log("hello");
  }

  ngOnInit() {
    this.user = {

      // this will just take a snapshot of the instance and initialize to the vars
      // so it will not update as the route changes
      id: this.route.snapshot.params['id'],
      name: this.route.snapshot.params['name'],
    };

    // this will change the users data once the params are changed within the components
    this.route.params.subscribe((params:Params) => {
      this.user.id = params['id'];
      this.user.name = params['name'];
    });
  }
}
