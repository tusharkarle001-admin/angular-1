import { Component, OnDestroy, OnInit } from '@angular/core';
import { interval, Observable, observable, Subscription } from 'rxjs';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent implements OnInit, OnDestroy {
  private firstObservation: Subscription;
  constructor() {}

  ngOnInit() {
    /* interval obserable */

    // interval is the obserable that is fired every 1000msec  which is subscribed then its arrow function is called
    // here the firstobservation is
    /*  this.firstObservation=interval(1000).subscribe((count) => {
      console.log(count);
    }); */

    // to create a custom obseravle
    const customIntervalObservable = Observable.create((observer: any) => {
      let count = 0;
      setInterval(() => {
        observer.next(count);
        if (count === 2) {
          observer.complete();
        }

        if (count > 4) {
          // create an observabe error if the conditon is true and it will stop observer
          observer.error(new Error('count is greater 3'));
        }
        count++;
      }, 1000);
    });

    // to subscribe the custom obserable
    customIntervalObservable.subscribe(
      (data: any) => {
        console.log(data);
      },
      () => {
        console.log('observer completed');
      }
    );
  }

  ngOnDestroy(): void {
    // here the obseravle is unsubscribed ie it will get stopped or not linked to the firstobservation
    this.firstObservation.unsubscribe();
  }
}
