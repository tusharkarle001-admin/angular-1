import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  serverElements: { type?: string; name: string; content: string }[] = [];

  onServerAdded(data: { name: string; content: string }) {
    this.serverElements.push({
      type: 'server',
      name: data.name,
      content: data.content,
    });
  }

  onAddBlueprintAdded(data: { name: string; content: string }) {
    this.serverElements.push({
      type: 'Blueprint',
      name: data.name,
      content: data.content,
    });
  }
}
