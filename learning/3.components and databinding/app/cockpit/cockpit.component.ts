import {
  Component,
  OnInit,
  EventEmitter,
  Output,
  ViewChild,
  ElementRef,
} from '@angular/core';

@Component({
  selector: 'app-cockpit',
  templateUrl: './cockpit.component.html',
  styleUrls: ['./cockpit.component.css'],
})
export class CockpitComponent implements OnInit {
  newServerName: string = '';
  newServerContent: string = '';

  // create an events
  @Output() serverCreated: EventEmitter<{ name: string; content: string }> =
    new EventEmitter();
  @Output() bluePrintCreated: EventEmitter<{
    name: string;
    content: string;
  }> = new EventEmitter();
  constructor() {}

  ngOnInit(): void {}

  //custom functions

  onAddServer() {
    this.serverCreated.emit({
      name: this.newServerName,
      content: this.newServerContent,
    });
  }

  onAddBlueprint() {
    this.bluePrintCreated.emit({
      name: this.newServerName,
      content: this.newServerContent,
    });
  }

  // using of the viewchild() decorator and local referecne
  @ViewChild('localRefereceId') serverContentInput: ElementRef;
  getLocalReferecence(idValue: string) {
    console.log(idValue);
    console.log(`this is viewchild` +this.serverContentInput.nativeElement.value);
  }
}
