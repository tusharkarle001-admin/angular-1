import { Component, Input, OnInit } from '@angular/core';
import { SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-server-element',
  templateUrl: './server-element.component.html',
  styleUrls: ['./server-element.component.css'],
})
export class ServerElementComponent implements OnInit {
  @Input() element: any;

  constructor() {
    console.log('constructor called');
  }

  ngOnInit() {
    console.log('ngoninit called');
  }

  ngOnChanges(changes: SimpleChanges): void {
    console.log('onchanges called');
    console.log(changes)

  }
}
