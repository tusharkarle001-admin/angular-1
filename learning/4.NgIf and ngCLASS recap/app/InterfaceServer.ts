export interface Task {
  name: string;
  content: string;
}
