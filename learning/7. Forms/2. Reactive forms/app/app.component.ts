import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validator, Validators } from '@angular/forms';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  title: string = 'Reactive Forms';
  // create an object/ instance of formGroup
  loginForm = new FormGroup({
    // there will be default value in the braccker
    // user: new FormControl('Default Name'),
    user: new FormControl('', [Validators.required, Validators.email]),
    password: new FormControl('', [
      Validators.required,
      Validators.minLength(5),
    ]),
  });
  constructor() {}

  ngOnInit(): void {}

  loginUser() {
    console.log(this.loginForm.value);
  }

  get usergetter() {
    return this.loginForm.get('user');
  }

  get passwordGetter() {
    return this.loginForm.get('password');
  }
}
