import {
  HttpInterceptor,
  HttpEvent,
  HttpHandler,
  HttpRequest,
  HttpEventType,
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, tap } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class AuthInterceptorService implements HttpInterceptor {
  // interceptor will run right before the req leaves our application
  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    console.log('request is on the way');

    console.log(req.url);
    /* the req.url or req.header ie req is inmutable ie un changable so we have to clone it to modify request */
    // modified request

    const modifiedRequest = req.clone({
      headers: req.headers.append('Auth', 'xyz'),
    });

    // to let the req leave the app
    // return next.handle(req);
    // return next.handle(modifiedRequest);

    // ------------------------
    // return next.handle(modifiedRequest).pipe(tap(event => {
    //   console.log(event);
    //   if (event.type === HttpEventType.Response) {
    //     console.log("Response arrived , body data:- ");
    //     console.log(event.body);

    //   }
    // }));

    // ----------------
    return next.handle(modifiedRequest);
  }
}
