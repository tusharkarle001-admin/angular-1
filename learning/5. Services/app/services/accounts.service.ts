import { Injectable ,EventEmitter } from '@angular/core';
import { LoggingService } from '../logging.service';

@Injectable({
  providedIn: 'root',
})
export class AccountsService {
  accounts = [
    {
      name: 'Master Account',
      status: 'active',
    },
    {
      name: 'Testaccount',
      status: 'inactive',
    },
    {
      name: 'Hidden Account',
      status: 'unknown',
    },
  ];

  constructor() {}

  addAccount(name: string, status: string) {
    this.accounts.push({ name: name, status: status });
  }

  StatusChange(id: number, newStatus: string) {
    this.accounts[id].status = newStatus;
  }

  // cross components data communication
  statusUpdated: EventEmitter<string> = new EventEmitter<string>();
}
