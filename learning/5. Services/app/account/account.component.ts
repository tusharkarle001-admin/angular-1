import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { LoggingService } from '../logging.service';
import { AccountsService } from '../services/accounts.service';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.css'],
})
export class AccountComponent implements OnInit {
  @Input() account: { name: string; status: string };
  @Input() id: number;

  onSetTo(status: string) {
    this.accountsService.StatusChange(this.id, status);
    // console.log('A server status changed, new status: ' + status);
    this.accountsService.statusUpdated.emit(status);
  }
  constructor(
    private LogService: LoggingService,
    private accountsService: AccountsService
  ) {}

  ngOnInit(): void {}
}
