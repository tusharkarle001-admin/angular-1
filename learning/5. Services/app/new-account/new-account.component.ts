import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { LoggingService } from '../logging.service';
import { AccountsService } from '../services/accounts.service';
@Component({
  selector: 'app-new-account',
  templateUrl: './new-account.component.html',
  styleUrls: ['./new-account.component.css'],
})
export class NewAccountComponent implements OnInit {
  @Output() accountAdded = new EventEmitter<{ name: string; status: string }>();

  onCreateAccount(accountName: string, accountStatus: string) {
    this.accountsService.addAccount(accountName, accountStatus);
    this.LogService.logStatusChange(accountStatus);
  }
  constructor(
    private LogService: LoggingService,
    private accountsService: AccountsService
  ) {
    
    this.accountsService.statusUpdated.subscribe((status: string) => {
      alert('new status' + status);
    });
  }

  ngOnInit(): void {}
}
