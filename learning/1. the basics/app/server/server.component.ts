import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-server',
  templateUrl: './server.component.html',
  styleUrls: ['./server.component.css'],
})
export class ServerComponent implements OnInit {
  // string interpolition

  serverId: number = 10;
  serverStatus: string = 'connected';
  getServerStatus() {
    return this.serverStatus;
  }

  //property binding
  allowNewServer: boolean = false;
  constructor() {
    setTimeout(() => {
      this.allowNewServer = true;
    }, 2000);

    // create a random value
  }

  ngOnInit(): void {}

  // event binding

  serverCreationStatus: string = 'no sever is created';

  onCreateServer() {
    this.serverCreationStatus = 'the server was created';
  }
  // <!--passing and using the data with event binding-- >

  // here by default values are not shown in the input tag
  serverInput: string = 'tre';
  onUpdateServerName(data: string) {
    this.serverInput = data;
  }

  // two way binding  using form module --> ngmodel
  // here by default values are shown in the input tag
  twoWayBindingData: string = 'testserver';

  // <!-- -------------------------------------- Directives ------------------------------------------ -->

  serverCreated: boolean = false;

  // <!-- -------------------------------------- using ng style ------------------------------------------ -->

  getColor() {
    return 'blue';
  }

  // <!-- -------------------------------------- using ng class ------------------------------------------ -->

  classStatus: string = 'online';

  /* for derivatives */

  servers: string[] = ['TestServer1', 'TestServer2', 'TestServer2'];
}
