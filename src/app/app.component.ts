import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map, Observable, catchError } from 'rxjs';
import { Post } from './post.model';
import { PostsService } from './posts.service';

/* interceptors are used to modify the request before leaving the app and response before
recieving to the app */

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  loadedPosts: any = [];
  error = null;
  isfetching: boolean = false;

  constructor(private http: HttpClient, private postService: PostsService) {}

  ngOnInit() {
    // when ever the page loades then fetch the posts
    this.onFetchPosts();
  }

  onCreatePost(postData: Post) {
    // Send Http request through services

    this.postService.createAndStorePost(postData);
  }

  onFetchPosts() {
    this.isfetching = true;
    // Send Http request

    // this.http
    //   .get('https://lucid-burner-337115-default-rtdb.firebaseio.com/posts.json')
    //   .subscribe((responseData) => {
    //     console.log(responseData);
    //   });

    // transforming the response data
    // this.postService.fetchPosts().subscribe((postsResponse: any) => {
    //   this.isfetching = false;
    //   this.loadedPosts = postsResponse;
    // });

    this.postService.fetchPosts().subscribe(
      (postsResponse: any) => {
        this.isfetching = false;
        this.loadedPosts = postsResponse;
      },
      (funError) => {
        this.error = funError;
        console.log(funError);
      }
    );
  }

  onClearPosts() {
    // Send Http request
    this.postService.deletePosts().subscribe(() => {
      this.loadedPosts = [];
    });
  }
}
